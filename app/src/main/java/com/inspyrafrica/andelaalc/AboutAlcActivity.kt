package com.inspyrafrica.andelaalc

import android.annotation.SuppressLint
import android.net.http.SslError
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.SslErrorHandler
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.activity_about_alc.*

class AboutAlcActivity : AppCompatActivity() {

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_alc)

        wv_alc.let {
            it.webViewClient = object : WebViewClient() {
                override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
                    var message : String = "SSL Certificate error."
                    when (error?.primaryError) {
                        SslError.SSL_UNTRUSTED -> {
                            message = "The certificate authority is not trusted."
                        }
                        SslError.SSL_DATE_INVALID ->{
                            message = "Invalid"
                        }
                        SslError.SSL_EXPIRED -> {
                            message = "The certificate has expired."
                        }
                        SslError.SSL_IDMISMATCH -> {
                            message = "The certificate Hostname mismatch."
                        }
                    }

                    message += "\"SSL Certificate Error\" Do you want to continue anyway?.. YES"

                    Log.d("SSLError",message)

                    handler!!.proceed()
                    Log.d("AboutActivity", "SSL ERROR")

                }
             }
            it.settings.javaScriptEnabled = true
            it.loadUrl("https://andela.com/alc")
        }
    }
}
